package Exercise;



import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws ParseException {
        ConnectionHandeler connectionHandeler = new ConnectionHandeler();
        StringBuilder fullUrl = new StringBuilder();
        StringBuilder fullUrl1 = new StringBuilder();
        System.out.println(">> ******************************************************");
        System.out.println(">> *****************     WELCOME    *********************");
        System.out.println(">> ******************************************************");
        Scanner scanner = new Scanner(System.in);

        System.out.println(">> Pick an option:\n" +
                ">> (1) Show a pokemon Info\n" +
                ">> (2) Show a location info\n" +
                ">> (3) Save and Quit");
        //Scanner scanner=new Scanner(System.in);
        int choseOption = scanner.nextInt();

        if (choseOption == 1) {
            System.out.println("Enter a Pokemon :");
            String pokemon = scanner.next();
            String url = "https://pokeapi.co/api/v2/pokemon/";
            fullUrl.append(url);
            fullUrl.append(pokemon);
            JSONParser parser = new JSONParser();

            JSONObject jobj = (JSONObject) parser.parse(connectionHandeler.makeHTTPRequest(fullUrl.toString()));
            System.out.println(connectionHandeler.printPokemonInfo(jobj));


        } else if (choseOption == 2) {
            System.out.println("Enter a Pokemon location :");
            String locationUrl = "https://pokeapi.co/api/v2/location/";
            String location = scanner.next();
            fullUrl1.append(locationUrl);
            fullUrl1.append(location);
            JSONParser parser1 = new JSONParser();
            JSONObject jasonobj = (JSONObject) parser1.parse(connectionHandeler.makeHTTPRequest(fullUrl1.toString()));
            System.out.println(connectionHandeler.printLocationInfo(jasonobj));

        } else if (choseOption == 3)
            System.out.println("*********************************************************************");
        System.out.println("Thank you for use the Application");
        System.out.println("*********************************************************************");
        scanner.close();
        System.exit(0);


    }
}


























