package Exercise;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ConnectionHandeler {

    public  String makeHTTPRequest(String file) {
        //String urlString = "https://pokeapi.co/api/v2/location/lavender-town";
        String response = null;
        try {
            URL url = new URL(file);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36");
            // int status = connection.getResponseCode();
            connection.connect();
            Scanner responseScanner = new Scanner(connection.getInputStream());
            while (responseScanner.hasNext()) {
                response = responseScanner.nextLine();
            }
        } catch ( IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String printPokemonInfo(JSONObject jobj){
        String name = (String) jobj.get("name");
        long id = (long) jobj.get("id");
        long hight = (long) jobj.get("height");
        long weight = (long) jobj.get("weight");
        String info = "Name: "+name+ "\nId: "+id+ "\nHight: "+hight+"\nWeight: "+weight;

        return info;
    }

    public String printLocationInfo(JSONObject jobj){
          StringBuilder builder =new StringBuilder();
        JSONArray jsonArray = (JSONArray)jobj.get("names");
        JSONObject regionObject= (JSONObject) jobj.get("region");
        //JSONObject regionName= (JSONObject) regionObject.get("name");
        String regionName = (String)regionObject.get("name");
         ArrayList<String> list=new ArrayList<>();
        for(int i=0; i<jsonArray.size();i++){
            JSONObject jo= (JSONObject) jsonArray.get(i);
            JSONObject oj= (JSONObject) jo.get("language");
            String name = (String) oj.get("name");
             list.add(name);

         }
        //builder.append(list);
        String s= "Region Name:"+regionName+" ";
        String l= "\nlanguage are :";
        builder.append(s);
        builder.append(l);
        for(int i=0;i<list.size();i++){
            String line="  "+list.get(i);
            builder.append(line);
        }
        return builder.toString();

    }
}
